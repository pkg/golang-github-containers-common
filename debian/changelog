golang-github-containers-common (0.50.1+ds1-4+apertis2) apertis; urgency=medium

  * Disable failing test pkg/supplemented.
    This test is failing after decoupling GPGME support. Since we do not use
    GPGME, disable the test

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 02 Nov 2023 21:17:46 +0530

golang-github-containers-common (0.50.1+ds1-4+apertis1) apertis; urgency=medium

  * Move to target repository. Needed for runtime dependency of libpod

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Wed, 25 Oct 2023 16:31:36 +0530

golang-github-containers-common (0.50.1+ds1-4+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 13 Jul 2023 10:42:59 +0000

golang-github-containers-common (0.50.1+ds1-4) unstable; urgency=medium

  * Tighten dependency on containers/image-spec and avoid distro patches

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 05 Feb 2023 09:55:56 -0500

golang-github-containers-common (0.50.1+ds1-3) unstable; urgency=medium

  * Team upload

  [ Reinhard Tartler ]
  * Tighten build depends

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.
  * Set upstream metadata fields: Repository, Security-Contact.

  [ Shengjing Zhu ]
  * Update section to golang
  * Replace transitional package in Build-Depends and Depends
    + golang-logrus-dev -> golang-github-sirupsen-logrus-dev
    + golang-toml-dev -> golang-github-burntsushi-toml-dev
    And format with wrap-and-sort
  * Update dependencies according to go.mod
  * Add Multi-Arch hint

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 30 Jan 2023 18:34:42 +0800

golang-github-containers-common (0.50.1+ds1-2) unstable; urgency=medium

  * upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 24 Nov 2022 09:17:47 -0500

golang-github-containers-common (0.50.1+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 11 Nov 2022 20:23:54 -0500

golang-github-containers-common (0.49.1+ds1-1) experimental; urgency=medium

  * New upstream release
  * Tighten dependencies on containers/{storage,image}
  * disable tests for pkg/cgroups

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 17 Aug 2022 10:02:15 +0200

golang-github-containers-common (0.48.0+ds1-2) experimental; urgency=medium

  * Recommend container-network-stack, prefer netavark

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 05 Aug 2022 14:54:07 +0200

golang-github-containers-common (0.48.0+ds1-1) experimental; urgency=medium

  * New upstream release
  * refresh patches
  * tighten dependencies
  * Revert "libimage: import should not ignore configured variant if any"
  * exclude tests that require ginkgo/v2

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 15 May 2022 17:44:07 -0400

golang-github-containers-common (0.47.2+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 08 Feb 2022 20:36:15 -0500

golang-github-containers-common (0.46.0+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 11 Jan 2022 20:10:43 -0500

golang-github-containers-common (0.44.5+ds1-1) unstable; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 13 Apr 2022 21:16:46 -0400

golang-github-containers-common (0.44.4+ds1-1) unstable; urgency=medium

  [ Arnaud Rebillout ]
  * Sync debian/shortnames.conf with upstream

  [ Reinhard Tartler ]
  * Explicitly build-depend on golang-github-coreos-go-systemd-dev

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 22 Dec 2021 22:21:19 -0500

golang-github-containers-common (0.44.3+ds1-2) unstable; urgency=medium

  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 25 Oct 2021 19:53:44 -0400

golang-github-containers-common (0.44.3+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 08 Oct 2021 16:55:47 -0400

golang-github-containers-common (0.44.0+ds1-1) experimental; urgency=medium

  * New upstream release
  * Drop manpage.patch, merged upstream
  * Tighten dependencies

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 06 Oct 2021 18:04:20 -0400

golang-github-containers-common (0.42.1+ds1-2) unstable; urgency=medium

  * debian/control:
    - Tighten dependency on containers/image
    - Bump standards version
  * debian/watch: fix github url pattern
  * Fix section in the generated manpage

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 04 Sep 2021 09:56:21 +0200

golang-github-containers-common (0.42.1+ds1-1) unstable; urgency=medium

  * New upstream release
  * Use distro copy of golang-github-jinzhu-copier-dev
  * Add gpg for tests
  * disable the passdriver tests
  * Bump dependency on runc

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 29 Aug 2021 08:33:45 +0200

golang-github-containers-common (0.38.16+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 20 Jul 2021 08:22:16 -0400

golang-github-containers-common (0.38.12+ds1-2) experimental; urgency=medium

  * golang-github-containers-common-dev: depend on disiqueria/gotree

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 28 Jun 2021 22:09:10 -0400

golang-github-containers-common (0.38.12+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 28 Jun 2021 08:22:16 -0400

golang-github-containers-common (0.38.9+ds1-1) experimental; urgency=medium

  * New upstream release
  * Disable LTO

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 10 Jun 2021 19:33:26 -0400

golang-github-containers-common (0.38.5+ds2-1) experimental; urgency=medium

  * New upstream release, temporarily vendored libraries:
    - github.com/jinzhu/copier
    - github.com/disiqueira/gotree

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 05 Jun 2021 21:59:53 -0400

golang-github-containers-common (0.36.0+ds1-1) experimental; urgency=medium

  * New upstream release
  * Install /usr/share/containers/seccomp.json,
    thanks to Laurent Bigonville (Closes: #988443).

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 28 May 2021 13:21:37 -0400

golang-github-containers-common (0.35.4+ds1-1) experimental; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 02 Apr 2021 17:53:25 -0400

golang-github-containers-common (0.33.4+ds1-1) unstable; urgency=medium

  * New upstream point release, only focused changes for podman 3.0

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 12 Feb 2021 08:56:18 -0500

golang-github-containers-common (0.34.2+ds1-1) experimental; urgency=medium

  * New upstream release
  * Tighten dependencies on golang-github-containers-storage

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 09 Feb 2021 10:32:03 -0500

golang-github-containers-common (0.33.4+ds1-1+deb11u2+apertis1) apertis; urgency=medium

  * Add manual license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Mon, 26 Jun 2023 15:10:24 +0530

golang-github-containers-common (0.33.4+ds1-1+deb11u2+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.
  * Fresh import for task T9696.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Sun, 25 Jun 2023 19:54:20 +0530

golang-github-containers-common (0.33.1+ds1-3) unstable; urgency=medium

  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 02 Feb 2021 14:49:53 -0500

golang-github-containers-common (0.33.1+ds1-2) experimental; urgency=medium

  * Force compilation against golang-go
  * Provide etc/containers/policy.json, formerly in the buildah package

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 30 Jan 2021 15:42:22 -0500

golang-github-containers-common (0.33.1+ds1-1) experimental; urgency=medium

  * New upstream release
  * Tighten dependencies on containers/storage, containers/image, runc and
    opencontainers/selinux

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 24 Jan 2021 09:13:51 -0500

golang-github-containers-common (0.28.1+ds1-1) experimental; urgency=medium

  * New upstream release
  * Backport upstream patch to fixup makefile generation

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 14 Dec 2020 12:05:45 -0500

golang-github-containers-common (0.26.3+ds1-3) unstable; urgency=medium

  * Provide etc/containers/policy.json, formerly in the buildah package

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 30 Jan 2021 10:30:10 -0500

golang-github-containers-common (0.26.3+ds1-2) unstable; urgency=medium

  * Install containers.conf(5)
  * Install shortnames.conf
  * Install registries.conf
  * Declare depends on golang-github-containers-image

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 02 Dec 2020 16:56:21 -0500

golang-github-containers-common (0.26.3+ds1-1) unstable; urgency=medium

  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 23 Nov 2020 17:41:23 -0500

golang-github-containers-common (0.22.0+ds1-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright: update using 'cme update dpkg-copyright'

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 22 Nov 2020 13:32:58 -0500

golang-github-containers-common (0.14.10+ds1-1) unstable; urgency=medium

  * New upstream version 0.14.10+ds1

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 14 Sep 2020 13:25:26 -0400

golang-github-containers-common (0.14.6+ds1-1) unstable; urgency=medium

  * New upstream version

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 26 Jul 2020 10:56:06 -0400

golang-github-containers-common (0.14.4+ds1-2) unstable; urgency=medium

  * upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 20 Jul 2020 07:18:27 -0400

golang-github-containers-common (0.14.4+ds1-1) experimental; urgency=medium

  * New upstream release.
  * Add build depend on golang-github-containers-imgae-dev
  * Exclude cgroup tests

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 12 Jul 2020 21:37:05 -0400

golang-github-containers-common (0.8.1+ds1-2) experimental; urgency=medium

  * Tighten build-dependencies on containers-storage

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 12 Jul 2020 21:21:03 -0400

golang-github-containers-common (0.8.1+ds1-1) experimental; urgency=medium

  * Initial release (Closes: #958426)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 21 Apr 2020 17:00:29 -0400
